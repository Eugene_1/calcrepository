﻿using ClientDeliveringFormulas.ServiceReference1;

namespace ClientDeliveringFormulas
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculationServiceClient client = new CalculationServiceClient();
            client.Open();
            client.TakeArithmeticExpressionFromClient("c=2+3");
            client.Close();
        }
    }
}
