﻿using System.Data.Entity;

namespace CalculationService
{
    /// <summary>
    /// Контекст, используемый для взаимодействия с БД.
    /// </summary>
    public class CalculationResultContext : DbContext
    {
        /// <summary>
        /// Создаёт экземпляр CalculationResultContext.
        /// </summary>
        public CalculationResultContext() : base("DbConnection") { }

        /// <summary>
        /// Возвращает или задаёт набор данных, через который выполняется взаимодействие с таблицей из БД.
        /// </summary>
        public DbSet<CalculationResult> CalculationResults { get; set; }
    }
}