﻿namespace CalculationService
{
    /// <summary>
    /// Представляет результат вычисления арифметического выражения, сохраняемый в БД.
    /// </summary>
    public class CalculationResult
    {
        /// <summary>
        /// Возвращает или задаёт первичный ключ записи.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Возвращает или задаёт наименование результата арифметической операции.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Возвращает или задаёт значение результата арифметической операции.
        /// </summary>
        public double CalculatedValue { get; set; }
    }
}