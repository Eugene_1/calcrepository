﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CalculationService
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы Service1.svc или Service1.svc.cs в обозревателе решений и начните отладку.
    public class CalculationService : ICalculationService
    {
        #region Getting an Arithmetic Expression from a Client

        /// <summary>
        /// Принимает от клиента арифметическое выражение, результат которого требуется получить.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая принятое арифметическое выражение</param>
        public void TakeArithmeticExpressionFromClient(string arithmeticExpression)
        {
            string test = arithmeticExpression;
        }

        /// <summary>
        /// Отдаёт клиенту результат выполнения арифметического выражения.
        /// </summary>
        /// <param name="expressionResult">Текстовая строка, представляющая результат вычисления арифметического выражения</param>
        public string GiveArithmeticExpressionResultToClient(string expressionResult)
        {
            return string.Empty;
        }

        /*

        /// <summary>
        /// Асинхронно принимает от клиента арифметическое выражение, результат которого требуется получить.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая принятое арифметическое выражение</param>
        /// <returns>Задача, выполняющая приём от клиента арифметического выражения, результат которого требуется получить.</returns>
        [OperationContract]
        public async Task TakeArithmeticExpressionFromClientAsync(string arithmeticExpression)
        {
            await Task.Run(() =>
            {
            });
        }

        /// <summary>
        /// Асинхронно отдаёт клиенту результат выполнения арифметического выражения.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая результат вычисления арифметического выражения</param>
        /// <returns>Задача, выполняющая отдачу клиенту результата выполнения арифметического выражения.</returns>
        [OperationContract]
        public async Task<string> GiveArithmeticExpressionResultToClientAsync(string arithmeticExpression)
        {
            return await Task<string>.Run(() =>
            {
                return string.Empty;
            });
        }

        //*/

        #endregion

        #region Parsing
        #endregion

        #region Validation
        #endregion

        #region Interaction with Database

        /// <summary>
        /// Добавляет результат арифметической операции в базу данных.
        /// </summary>
        /// <param name="insertedValue">Новый результат, который требуется добавить в БД.</param>
        /// <returns>Добавленный в БД результат</returns>
        private CalculationResult Insert(CalculationResult insertedValue)
        {
            CalculationResult inserted = null;
            using (var dbContext = new CalculationResultContext())
            {
                if (Select(insertedValue.Name) == null)
                {
                    inserted = dbContext.CalculationResults.Add(insertedValue);
                    int x = dbContext.SaveChanges();
                }
            }
            return inserted;
        }

        /// <summary>
        /// Выбирает требуемый результат арифметической операции из БД.
        /// </summary>
        /// <param name="selectedName">Наименование результата, который требуется извлечь из БД.</param>
        /// <returns>Выбранный из БД результат арифметической операции</returns>
        CalculationResult Select(string selectedName)
        {
            CalculationResult selected = null;
            using (var dbContext = new CalculationResultContext())
            {
                selected = dbContext.CalculationResults.Where(s => s.Name == selectedName).FirstOrDefault();
            }
            return selected;
        }

        /// <summary>
        /// Изменяет значение уже существующего в БД результата арифметической операции.
        /// </summary>
        /// <param name="newValue">Новое значение для результата.</param>
        /// <returns>Количество изменённых записей в БД</returns>
        int Update(CalculationResult newValue)
        {
            int recordsUpdated = 0;
            using (var dbContext = new CalculationResultContext())
            {
                var updated = dbContext.CalculationResults.First(s => s.Name == newValue.Name);
                if (updated != null)
                {
                    updated.Name = newValue.Name;
                    recordsUpdated = dbContext.SaveChanges();
                }
            }
            return recordsUpdated;
        }

        #endregion

        #region Arithmetic Operations

        /// <summary>
        /// Выполняет сложение.
        /// </summary>
        /// <param name="arg1">Первое слагаемое</param>
        /// <param name="arg2">Второе слагаемое</param>
        /// <returns>Сумма</returns>
        private double Add(double arg1, double arg2)
        {
            return arg1 + arg2;
        }

        /// <summary>
        /// Выполняет вычитание.
        /// </summary>
        /// <param name="arg1">Уменьшаемое</param>
        /// <param name="arg2">Вычитаемое</param>
        /// <returns>Разность</returns>
        private double Sub(double arg1, double arg2)
        {
            return arg1 - arg2;
        }

        /// <summary>
        /// Выполняет умножение.
        /// </summary>
        /// <param name="arg1">Первый сомножитель</param>
        /// <param name="arg2">Второй сомножитель</param>
        /// <returns>Произведение.</returns>
        private double Mul(double arg1, double arg2)
        {
            return arg1 * arg2;
        }

        /// <summary>
        /// Выполняет деление.
        /// </summary>
        /// <param name="arg1">Делимое</param>
        /// <param name="arg2">Делитель</param>
        /// <returns>Частное</returns>
        private double Div(double arg1, double arg2)
        {
            return arg1 / arg2;
        }

        #endregion
    }
}
