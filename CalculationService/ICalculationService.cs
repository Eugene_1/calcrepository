﻿using System.ServiceModel;
using System.Threading.Tasks;

namespace CalculationService
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService1" в коде и файле конфигурации.
    [ServiceContract]
    public interface ICalculationService
    {
        #region Getting an Arithmetic Expression from a Client

        /// <summary>
        /// Принимает от клиента арифметическое выражение, результат которого требуется получить.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая принятое арифметическое выражение</param>
        [OperationContract]
        void TakeArithmeticExpressionFromClient(string arithmeticExpression);

        /// <summary>
        /// Отдаёт клиенту результат выполнения арифметического выражения.
        /// </summary>
        /// <param name="expressionResult">Текстовая строка, представляющая результат вычисления арифметического выражения</param>
        [OperationContract]
        string GiveArithmeticExpressionResultToClient(string expressionResult);

        /*
        /// <summary>
        /// Асинхронно принимает от клиента арифметическое выражение, результат которого требуется получить.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая принятое арифметическое выражение</param>
        /// <returns>Задача, выполняющая приём от клиента арифметического выражения, результат которого требуется получить.</returns>
        [OperationContract]
        Task TakeArithmeticExpressionFromClientAsync(string arithmeticExpression);

        /// <summary>
        /// Асинхронно отдаёт клиенту результат выполнения арифметического выражения.
        /// </summary>
        /// <param name="arithmeticExpression">Текстовая строка, представляющая результат вычисления арифметического выражения</param>
        /// <returns>Задача, выполняющая отдачу клиенту результата выполнения арифметического выражения.</returns>
        [OperationContract]
        Task<string> GiveArithmeticExpressionResultToClientAsync(string arithmeticExpression);
        //*/

        #endregion
    }
}
